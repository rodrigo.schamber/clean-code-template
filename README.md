# CLEAN-CODE-TEMPLATE
## _Clean Code Practice Assignment_

This code has the purpose to demonstrate some clean code practices.

## Separation of Concerns

- Built in two different layers (concerns)
- UI Components
- Data Access (Gateways)

## Low Coupling and High Cohesion

- Data coupling (low coupling)
- Functional cohesion
- Different elements (functions) of the Functional Component App.js cooperate to achieve a single function to render the content.

## Patterns

- Low coupling;
- high cohesion;

## SOLID, DRY, KISS, YAGNI and others

- Single responsibility
- DRY
- KISS

## Installation

clean-code-template requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and start the server.

```sh
cd clean-code-template
yarn install
yarn start
```

## License

MIT