import React, {useEffect, useState} from 'react';
import {Card, Col, Container, Row} from 'react-bootstrap';
import NasaPicture from './components/NasaPicture';
import getPicture from './gateway/getPicture';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default function App() {
  
  const [picture, setPicture] = useState(new NasaPicture());
  const [startDate, setStartDate] = useState(new Date());
  
  function formatDate(dateToFormat){
  
    return`${dateToFormat.getFullYear()}-${(dateToFormat.getMonth()+1)}-${dateToFormat.getDate()}`
  
  };

  function handleDateChange(date){
  
    loadPicture(date);
    setStartDate(date);
  
  };

  function loadPicture(date){
    
    getPicture(formatDate(date)).then((item) => {
      setPicture(item);
    });
  
  };
  
  useEffect(() => {
    
    loadPicture(startDate);
  
  }, []);
  
  return (
    <Container fluid>
      <Row className="justify-content-lg-center">
        <Col lg={6}>
          <Card>
            <Card.Header>
              <Card.Title>Nasa Daily Picture</Card.Title>
            </Card.Header>
              <DatePicker
                selected={startDate}
                onChange={(date) => {
                  
                  handleDateChange(date);
                
                }}
              />
            <Card.Img variant="top" src={picture.url}/>
            <Card.Body>
              <Card.Text>
                {picture.title}
                <br></br>
                {picture.explanation}
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};