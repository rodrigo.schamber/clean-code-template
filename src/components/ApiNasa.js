export default class ApiNasa {
    constructor(apiKey, date, url){
        this.apiKey = apiKey;
        this.date = date;
        this.url = url;
    }
}